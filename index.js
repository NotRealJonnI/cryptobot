const TelegramBot = require('node-telegram-bot-api');
const request = require('request');

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('./testDB.db');
 
db.serialize(function() {
 /* db.run("CREATE TABLE lorem (info TEXT)");
 
  var stmt = db.prepare("INSERT INTO lorem VALUES (?)");
  for (var i = 0; i < 10; i++) {
      stmt.run("Ipsum " + i);
  }
  stmt.finalize();
  */
  db.each("SELECT rowid AS id, info FROM lorem", function(err, row) {
      console.log(row.id + ": " + row.info);
  });
});
 
db.close();

var myInt = setInterval(function () {
  console.log("Hello");
}, 500);//каждые 5 сек

//const schedule = require('node-schedule');
//function sendTime(time, msg, text) {
 //       new schedule.scheduleJob({ start: new Date(Date.now() + Number(time) * 1000 * 60), end: new Date(new Date(Date.now() + Number(time) * 1000 * 60 + 1000)), rule: '*/1 * * * * *' }, function () {
//            bot.sendMessage(msg.chat.id, text);
//        });
 //   }
/*
    bot.onText(/\/send/, msg => {
      sendTime(5,msg,'текст')
 })
 Отправить сообщение с текстом "текст" с ожиданием 5 минут при получении /send:
*/

const token = '782104257:AAGPgucaaX1aDp_Q1gng87Fe9Ja3K3h7G-A';
const bot = new TelegramBot(token, {polling: true});

bot.onText(/\/parametrs/, (msg, match) => {
  const chatId = msg.chat.id;
  bot.sendMessage(chatId, JSON.stringify(msg));
});

bot.onText(/\/hello/, (msg, match) => {
  const chatId = msg.chat.id;
  const userName = msg.chat.username;
  bot.forwardMessage(chatId, "Hello "+ userName);
});

bot.onText(/\/curse/, (msg, match) => {
  const chatId = msg.chat.id;

  bot.sendMessage(chatId, 'Выберите какая валюта вас интересует', {
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: '€ - EUR',
            callback_data: 'EUR'
          }, {
            text: '$ - USD',
            callback_data: 'USD'
          }, {
            text: '₽ - RUR',
            callback_data: 'RUR'
          }, {
            text: '₿ - BTC',
            callback_data: 'BTC'
          }
        ]
      ]
    }
  });
});

bot.on('callback_query', query => {
  const id = query.message.chat.id;

  request('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5', function (error, response, body) {
    const data = JSON.parse(body);
    const result = data.filter(item => item.ccy === query.data)[0];
    const flag = {
      'EUR': '🇪🇺',
      'USD': '🇺🇸',
      'RUR': '🇷🇺',
      'UAH': '🇺🇦',
      'BTC': '₿'
    }
    let md = `
      *${flag[result.ccy]} ${result.ccy} 💱 ${result.base_ccy} ${flag[result.base_ccy]}*
      Buy: _${result.buy}_
      Sale: _${result.sale}_
    `;
    bot.sendMessage(id, md, {parse_mode: 'Markdown'});
  })
})

